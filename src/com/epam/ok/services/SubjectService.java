package com.epam.ok.services;

import java.sql.SQLException;
import java.util.Set;

import com.epam.ok.dao.AbiturientDAO;
import com.epam.ok.dao.DAOCommand;
import com.epam.ok.dao.DAOManager;
import com.epam.ok.dao.MarkDAO;
import com.epam.ok.model.Abiturient;

public class SubjectService {
	private DAOManager daoManager;

	public SubjectService(DAOManager daoManager) {
		super();
		this.daoManager = daoManager;
	}
	
	@SuppressWarnings("unchecked")
	public Set<String> selectSubjectsByAbiturientLogin(String login) {
		try {
			return (Set<String>) daoManager.transaction(new DAOCommand() {
				@Override
				public Object execute(DAOManager daoManager) {
					try {
						AbiturientDAO abiturientDAO = daoManager.getAbiturientDAOTx();
						Abiturient abitur = abiturientDAO.selectAbiturientByLogin(login);
						MarkDAO markDAO = daoManager.getMarkDAOTx();
						Set<String> otherSubjects = markDAO.selectOtherMarksOfAbiturient(abitur.getID());
						return otherSubjects;
					} catch (SQLException e) {
						return null;
					}
				}
			});
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}
}
